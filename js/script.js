/* 
Теоретичні питання
1. Опишіть своїми словами що таке Document Object Model (DOM)
2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?
3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
4. Яка різниця між nodeList та HTMLCollection?

Практичні завдання
 1. Знайдіть всі елементи з класом "feature", запишіть в змінну, вивести в консоль.
 Використайте 2 способи для пошуку елементів. 
 Задайте кожному елементу з класом "feature" вирівнювання тексту по - центру(text-align: center).
 
 2. Змініть текст усіх елементів h2 на "Awesome feature".

 3. Знайдіть всі елементи з класом "feature-title" та додайте в кінець тексту елементу знак оклику "!".
 */

 /* 
 
 Теория

 1) DOM это по своему дерево объектов, где каждый объект является частью документа
 2) innerHTML в него можно передать тег, те при создании текста с помощью этого метода если текст находится в теге html он добавляет в верстку тег с указаным текстом
    innerText этот метод указывает все переданное содержимое в верстку (включая текст тега который передается)

3) к элементу можно обратиться -
 * document.getElementById(ищет элемент по ид)
 * document.getElementsByTagName(ищет элементы по тегу)
 * document.getElementsByClassName(ищет элементы по классу)
 * document.querySelector(ищет элемент по селектору - класс,ид,тег - возвращает первое совпадение) этот метод и метод ниже новее и удобнее в использовании
 * document.querySelectorAll(ищет элементы по селектору)
  
 4) nodeList - может содержать любые типы узлов и поддерживает метод forEach, HTMLCollection может содержать только элементы HTML

 */


 // Практика

 // 1 

 const searchFeature = document.querySelectorAll('.feature');
//  const searchFeature = document.getElementsByClassName('feature');

 console.log(searchFeature);

//  searchFeature.cssText = `textAlign: center;`

// 2 

const searchTitle = document.querySelectorAll('h2')

for (let key of searchTitle) {
    key.innerText = 'Awesome feature';
}

// 3 

const getClassTitle = document.querySelectorAll('.feature-title');

for (let key of getClassTitle) {
    key.textContent += '!';
}